package concurrency

import (
	"encoding/json"
	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/appengine/urlfetch"
)

const (
	fixerURLBase = "http://api.fixer.io/latest?base="
)

// fixerCurrency
// represent exchange rates
// from fixer.io
type fixerCurrency struct {
	Base  string             `json:"base"`
	Date  string             `json:"date"`
	Rates map[string]float64 `json:"rates"`
}

// convertedCurrency
// represent conversion results
type convertedCurrency struct {
	Amount    float64   `json:"amount" xml:"amount"`
	Currency  string    `json:"currency" xml:"currency"`
	Converted StringMap `json:"converted" xml:"converted"`
}

func getCurrency(c context.Context, baseCurrency string) (*fixerCurrency, error) {
	var currentFixer fixerCurrency
	client := urlfetch.Client(c)
	resp, err := client.Get(fixerURLBase + baseCurrency)
	if err != nil {
		return nil, err
	}
	decoder := json.NewDecoder(resp.Body)
	if err = decoder.Decode(&currentFixer); err != nil {
		return nil, fmt.Errorf("Can't Unmarshal fixer response: %s", err)
	}
	return &currentFixer, nil
}

func (f fixerCurrency) convert(amount float64) (convertedCurrency, error) {
	c := convertedCurrency{
		Amount:    amount,
		Currency:  f.Base,
		Converted: make(map[string]float64),
	}
	for currency, rate := range f.Rates {
		c.Converted[currency] = roundTill(rate*amount, 2)
	}
	return c, nil
}
