package concurrency

import "net/http"

var supportedCurrencies = map[string]bool{
	"AUD": true, "BGN": true, "BRL": true, "CAD": true,
	"CHF": true, "CNY": true, "CZK": true, "DKK": true,
	"GBP": true, "HKD": true, "HRK": true, "HUF": true,
	"IDR": true, "ILS": true, "INR": true, "JPY": true,
	"KRW": true, "MXN": true, "MYR": true, "NOK": true,
	"NZD": true, "PHP": true, "PLN": true, "RON": true,
	"RUB": true, "SEK": true, "SGD": true, "THB": true,
	"TRY": true, "USD": true, "ZAR": true, "EUR": true,
}

func init() {
	// static hello page
	http.HandleFunc("/", indexHandler)
	// convert page handler
	http.HandleFunc("/convert", convertHandler)
}
